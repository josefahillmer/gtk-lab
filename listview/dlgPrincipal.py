#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgPrincipal():
    def __init__(self, titulo=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.dialogo = self.builder.get_object("dlgEjemplo")
        self.dialogo.set_title(titulo)
        self.dialogo.resize(600, 400)

        # boton cancelar
        boton_cancelar = self.dialogo.add_button(Gtk.STOCK_CANCEL,
                                                 Gtk.ResponseType.CANCEL)
        boton_cancelar.set_always_show_image(True)
        boton_cancelar.connect("clicked", self.boton_cancelar_clicked)

        # boton aceptar
        boton_aceptar = self.dialogo.add_button(Gtk.STOCK_OK,
                                                Gtk.ResponseType.OK)
        boton_aceptar.set_always_show_image(True)
        boton_aceptar.connect("clicked", self.boton_aceptar_clicked)

        # cuadros de texto
        self.nombre = self.builder.get_object("nombre")
        self.apellido = self.builder.get_object("apellido")

    def boton_cancelar_clicked(self, btn=None):
        self.dialogo.destroy()

    def boton_aceptar_clicked(self, btn=None):
        return
