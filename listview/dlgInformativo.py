#!/usr/bin/env python3{}
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgInformativo():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.inf = self.builder.get_object("dlgInformativo")
        self.inf.resize(600, 400)

        # Se añade mensaje en el cuadro de advertencia
        self.inf.set_property("text", "¡DATOS GUARDADOS!")
        self.inf.set_property("secondary_text",
                              "Los datos se han añadido de forma correcta")

        # Se crean el boton Ok
        self.inf.add_button(button_text="OK", response_id=Gtk.ResponseType.OK)
        self.inf.connect("response", self.click_ok)

        self.inf.show_all()

    def click_ok(self, widget, response_id):
        print("response_id is", response_id)
        # destruye el widget (el diálogo) cuando se llama a la función click_ok
        widget.destroy()
