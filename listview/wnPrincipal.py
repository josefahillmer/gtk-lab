# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dlgPrincipal import dlgPrincipal
from dlgAdvertencia import dlgAdvertencia
from dlgEliminar import dlgEliminar
from dlgInformativo import dlgInformativo


class wnPrincipal():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.window = self.builder.get_object("wnPrincipal")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Ventana Ejemplo")
        self.window.maximize()
        self.window.show_all()

        # botones de menú
        boton_aceptar = self.builder.get_object("btnAnadir")
        boton_aceptar.connect("clicked", self.boton_aceptar_clicked)
        boton_editar = self.builder.get_object("btnEditar")
        boton_editar.connect("clicked", self.boton_editar_clicked)
        boton_eliminar = self.builder.get_object("btnEliminar")
        boton_eliminar.connect("clicked", self.boton_eliminar_clicked)

        # label
        self.label_changed = self.builder.get_object("labelChanged")

        # liststore
        self.liststore = self.builder.get_object("treeEjemplo")
        self.liststore.connect("cursor-changed", self.liststore_changed)
        # self.model = Gtk.ListStore(str, str)
        self.model = Gtk.ListStore(*(2 * [str]))
        self.liststore.set_model(model=self.model)

        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Nombre",
                                    cell_renderer=cell,
                                    text=0)

        self.liststore.append_column(column)

        cell_edit = Gtk.CellRendererText()
        cell_edit.set_property("editable", True)
        cell_edit.set_property("scale", 1.0)
        # cell_edit.set_property("background", "red")
        cell_edit.connect("edited", self.cell_text_apellido_edited)

        column = Gtk.TreeViewColumn(title="Apellido",
                                    cell_renderer=cell_edit,
                                    text=1)
        cell_edit = Gtk.CellRendererText()
        cell_edit.set_property("editable", True)
        cell_edit.set_property("scale", 1.0)
        # cell_edit.set_property("background", "red")
        cell_edit.connect("edited", self.cell_text_apellido_edited)

        column = Gtk.TreeViewColumn(title="Apellido",
                                    cell_renderer=cell_edit,
                                    text=1)
        self.liststore.append_column(column)

        self.model.append(["Fabio", "Durán"])
        self.model.append(["Pepito", "Pepón"])

    def liststore_changed(self, tree=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        nombre = model.get_value(it, 0)
        nombre = ''.join(["Nombre: ",
                          nombre])
        apellido = model.get_value(it, 1)
        self.label_changed.set_text(nombre + apellido)

    def cell_text_apellido_edited(self, cell_renderer, path, text):
        self.model[path][1] = text
        # Los espacios de nombre y apellido estan vacios se va a dlgAdvertencia
        if text == "":
            dlgAdvertencia()
            self.model[path][1] = "XXXXX"

    def boton_aceptar_clicked(self, btn=None):
        dlg = dlgPrincipal(titulo="Añadir persona")
        response = dlg.dialogo.run()

        if response == Gtk.ResponseType.OK:
            nombre = dlg.nombre.get_text()
            apellido = dlg.apellido.get_text()

        # Los espacios de nombre y apellido estan vacios se va a dlgAdvertencia
        if nombre == "" or apellido == "":
            dlgAdvertencia()
            dlg.dialogo.destroy()
        # Si no se añade el nuevo nombre y apellido
        else:
            self.model.append([nombre, apellido])
            dlg.dialogo.destroy()
            # Para mosntrar la ventana de añadido correctamente
            dlgInformativo()

    def boton_editar_clicked(self, btn=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        dlg = dlgPrincipal()
        dlg.nombre.set_text(model.get_value(it, 0))
        dlg.apellido.set_text(model.get_value(it, 1))

        response = dlg.dialogo.run()
        if response == Gtk.ResponseType.OK:
            # Los espacios de nombre y apellido no pueden estar vacios
            if  dlg.nombre.get_text() == "" or dlg.apellido.get_text() == "":
                advertencia = dlgAdvertencia()
                dlg.dialogo.destroy()
            else:
                model.set_value(it, 0, dlg.nombre.get_text())
                model.set_value(it, 1, dlg.apellido.get_text())
                dlg.dialogo.destroy()

    def boton_eliminar_clicked(self, btn=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        dlgE = dlgEliminar()
        response_id = dlgE.eli.run()
        # El id de aceptar para eliminar es -8
        if response_id == -8:
            model.remove(it)
        else:
            pass


if __name__ == "__main__":
    PRINCIPAL = wnPrincipal()
    Gtk.main()
