#!/usr/bin/env python3{}
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgAdvertencia():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.ad = self.builder.get_object("dlgAdvertencia")
        self.ad.resize(600, 400)

        # Se añade mensaje en el cuadro de advertencia
        self.ad.set_property("text",
                             "Los siguientes campos que no pueden ser vacios")
        self.ad.set_property("secondary_text", "*Nombre\n*Apellido")

        # Se crean el boton Ok
        self.ad.add_button(button_text="OK", response_id=Gtk.ResponseType.OK)
        self.ad.connect("response", self.click_ok)

        self.ad.show_all()

    def click_ok(self, widget, response_id):
        print("response_id is", response_id)
        # destruye el widget (el diálogo) cuando se llama a la función click_ok
        widget.destroy()
