# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgEliminar():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.eli = self.builder.get_object("dlgEliminar")
        self.eli.resize(600, 400)

        # Se añade mensaje en el cuadro de eliminar un elemento
        self.eli.set_property("text", "¡CUIDADO!")
        self.eli.set_property("secondary_text",
                              "¿Seguro que desea eliminar este elemento?")

        # Se crean los botones Yes y No
        boton_YES = self.eli.add_button(Gtk.STOCK_YES,
                                        Gtk.ResponseType.YES)
        boton_YES.connect("clicked", self.click_YES)
        boton_NO = self.eli.add_button(Gtk.STOCK_NO,
                                       Gtk.ResponseType.NO)
        boton_NO.connect("clicked", self.click_NO)

        self.eli.show_all()

    def click_YES(self, btn=0):
        self.eli.destroy()

    def click_NO(self, btn=0):
        self.eli.destroy()
